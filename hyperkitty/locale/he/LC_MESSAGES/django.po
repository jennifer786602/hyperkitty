# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-12-21 18:59+0530\n"
"PO-Revision-Date: 2020-12-21 20:06+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew <https://hosted.weblate.org/projects/gnu-mailman/"
"hyperkitty/he/>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 4.4-dev\n"

#: forms.py:64
msgid "Attach a file"
msgstr "צירוף קובץ"

#: forms.py:65
msgid "Attach another file"
msgstr "צירוף קובץ נוסף"

#: forms.py:66
msgid "Remove this file"
msgstr "הסרת הקובץ הזה"

#: templates/hyperkitty/404.html:28
msgid "Error 404"
msgstr "שגיאה 404"

#: templates/hyperkitty/404.html:30 templates/hyperkitty/500.html:31
msgid "Oh No!"
msgstr "הו לא!"

#: templates/hyperkitty/404.html:32
msgid "I can't find this page."
msgstr "אין אפשרות למצוא את העמוד הזה."

#: templates/hyperkitty/404.html:33 templates/hyperkitty/500.html:34
msgid "Go back home"
msgstr "חזרה לדף הבית"

#: templates/hyperkitty/500.html:29
msgid "Error 500"
msgstr "שגיאה 500"

#: templates/hyperkitty/500.html:33
msgid "Sorry, but the requested page is unavailable due to a server hiccup."
msgstr "העמוד המבוקש אינו זמין עקב שיהוק מצד השרת, עמך הסליחה."

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "started"
msgstr "החלה"

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "last active:"
msgstr "פעילות אחרונה:"

#: templates/hyperkitty/ajax/reattach_suggest.html:8
msgid "see this thread"
msgstr "הצגת השרשור הזה"

#: templates/hyperkitty/ajax/reattach_suggest.html:12
msgid "(no suggestions)"
msgstr "(אין הצעות)"

#: templates/hyperkitty/ajax/temp_message.html:11
msgid "Sent just now, not yet distributed"
msgstr "נשלח כרגע, טרם הופץ"

#: templates/hyperkitty/api.html:5
msgid "REST API"
msgstr "API מסוג REST"

#: templates/hyperkitty/api.html:7
msgid ""
"HyperKitty comes with a small REST API allowing you to programatically "
"retrieve emails and information."
msgstr ""
"ל־HyperKitty יש API מסוג REST שמאפשר לך לקבל הודעות דוא״ל ומידע באמצעות קוד."

#: templates/hyperkitty/api.html:10
msgid "Formats"
msgstr "תבניות"

#: templates/hyperkitty/api.html:12
msgid ""
"This REST API can return the information into several formats.  The default "
"format is html to allow human readibility."
msgstr ""
"ה־API הזה מסוג REST יכול להחזיר את המידע במגוון תבניות. תבנתי בררת המחדל היא "
"HTML כדי לאפשר לבני אדם לקרוא את הפלט."

#: templates/hyperkitty/api.html:14
msgid ""
"To change the format, just add <em>?format=&lt;FORMAT&gt;</em> to the URL."
msgstr ""
"כדי לשנות את התבנית, עליך להוסיף <em>?format=&lt;FORMAT&gt;</em> לכתובת."

#: templates/hyperkitty/api.html:16
msgid "The list of available formats is:"
msgstr "רשימת התבניות הזמינות היא:"

#: templates/hyperkitty/api.html:20
msgid "Plain text"
msgstr "טקסט פשוט"

#: templates/hyperkitty/api.html:26
msgid "List of mailing-lists"
msgstr "רשימה של רשימות דיוור (mailing-lists)"

#: templates/hyperkitty/api.html:27 templates/hyperkitty/api.html:33
#: templates/hyperkitty/api.html:39 templates/hyperkitty/api.html:45
#: templates/hyperkitty/api.html:51
msgid "Endpoint:"
msgstr "נקודת קצה:"

#: templates/hyperkitty/api.html:29
msgid ""
"Using this address you will be able to retrieve the information known about "
"all the mailing lists."
msgstr "עם הכתובת הזאת תהיה לך אפשרות לקבל את המידע הזמין על כל רשימות הדיוור."

#: templates/hyperkitty/api.html:32
msgid "Threads in a mailing list"
msgstr "שרשורים ברשימת דיוור"

#: templates/hyperkitty/api.html:35
msgid ""
"Using this address you will be able to retrieve information about all the "
"threads on the specified mailing list."
msgstr ""
"עם הכתובת הזאת תהיה לך אפשרות לקבל את כל המידע על השרשורים ברשימת דיוור "
"מסוימת."

#: templates/hyperkitty/api.html:38
msgid "Emails in a thread"
msgstr "הודעות בשרשור"

#: templates/hyperkitty/api.html:41
msgid ""
"Using this address you will be able to retrieve the list of emails in a "
"mailing list thread."
msgstr ""
"עם הכתובת הזו תהיה לך אפשרות לקבל את רשימת ההודעות בשרשור ברשימת דיוור."

#: templates/hyperkitty/api.html:44
msgid "An email in a mailing list"
msgstr "הודעה ברשימת דיוור"

#: templates/hyperkitty/api.html:47
msgid ""
"Using this address you will be able to retrieve the information known about "
"a specific email on the specified mailing list."
msgstr ""
"עם הכתובת הזאת תהיה לך אפשרות לקבל את המידע הזמין על הודעה מסוימת ברשימת "
"דיוור מסוימת."

#: templates/hyperkitty/api.html:50
msgid "Tags"
msgstr "תגיות"

#: templates/hyperkitty/api.html:53
msgid "Using this address you will be able to retrieve the list of tags."
msgstr "עם הכתובת הזאת תהיה לך אפשרות לקבל את רשימת התגיות."

#: templates/hyperkitty/base.html:56 templates/hyperkitty/base.html:111
msgid "Account"
msgstr "חשבון"

#: templates/hyperkitty/base.html:61 templates/hyperkitty/base.html:116
msgid "Mailman settings"
msgstr "הגדרות Mailman"

#: templates/hyperkitty/base.html:66 templates/hyperkitty/base.html:121
#: templates/hyperkitty/user_profile/base.html:17
msgid "Posting activity"
msgstr "פעילות פרסום"

#: templates/hyperkitty/base.html:71 templates/hyperkitty/base.html:126
msgid "Logout"
msgstr "יציאה"

#: templates/hyperkitty/base.html:77 templates/hyperkitty/base.html:133
msgid "Sign In"
msgstr "כניסה"

#: templates/hyperkitty/base.html:81 templates/hyperkitty/base.html:137
msgid "Sign Up"
msgstr "הרשמה"

#: templates/hyperkitty/base.html:90
msgid "Search this list"
msgstr "חיפוש ברשימה הזו"

#: templates/hyperkitty/base.html:90
msgid "Search all lists"
msgstr "חיפוש בכל הרשימות"

#: templates/hyperkitty/base.html:148
msgid "Manage this list"
msgstr "ניהול הרשימה הזו"

#: templates/hyperkitty/base.html:153
msgid "Manage lists"
msgstr "ניהול רשימות"

#: templates/hyperkitty/base.html:191
msgid "Keyboard Shortcuts"
msgstr "קיצורי מקלדת"

#: templates/hyperkitty/base.html:194
msgid "Thread View"
msgstr "תצוגת שרשור"

#: templates/hyperkitty/base.html:196
msgid "Next unread message"
msgstr "ההודעה הבאה שלא נקראה"

#: templates/hyperkitty/base.html:197
msgid "Previous unread message"
msgstr "ההודעה הקודמת שלא נקראה"

#: templates/hyperkitty/base.html:198
msgid "Jump to all threads"
msgstr "קפיצה לכל השרשורים"

#: templates/hyperkitty/base.html:199
msgid "Jump to MailingList overview"
msgstr "קפיצה לסקירת רשימת דיוור"

#: templates/hyperkitty/base.html:213
msgid "Powered by"
msgstr "מופעל על גבי"

#: templates/hyperkitty/base.html:213
msgid "version"
msgstr "גרסה"

#: templates/hyperkitty/errors/notimplemented.html:7
msgid "Not implemented yet"
msgstr "לא מוטמע עדיין"

#: templates/hyperkitty/errors/notimplemented.html:12
msgid "Not implemented"
msgstr "לא מוטמע"

#: templates/hyperkitty/errors/notimplemented.html:14
msgid "This feature has not been implemented yet, sorry."
msgstr "תכונה זו טרם הוטמעה, עמך הסליחה."

#: templates/hyperkitty/errors/private.html:7
msgid "Error: private list"
msgstr "שגיאה: רשימה פרטית"

#: templates/hyperkitty/errors/private.html:19
msgid ""
"This mailing list is private. You must be subscribed to view the archives."
msgstr "רשימת דיוור זו היא פרטית. עליך להיות מנוי כדי לצפות בארכיונים."

#: templates/hyperkitty/fragments/like_form.html:16
msgid "You like it (cancel)"
msgstr "אהבת את זה (ביטול)"

#: templates/hyperkitty/fragments/like_form.html:24
msgid "You dislike it (cancel)"
msgstr "לא אהבת את זה (ביטול)"

#: templates/hyperkitty/fragments/like_form.html:27
#: templates/hyperkitty/fragments/like_form.html:31
msgid "You must be logged-in to vote."
msgstr "עליך להיכנס למערכת כדי להצביע."

#: templates/hyperkitty/fragments/month_list.html:6
msgid "Threads by"
msgstr "שרשורים מאת"

#: templates/hyperkitty/fragments/month_list.html:6
msgid " month"
msgstr " חודש"

#: templates/hyperkitty/fragments/overview_threads.html:12
msgid "New messages in this thread"
msgstr "הודעות חדשות בשרשור הזה"

#: templates/hyperkitty/fragments/overview_threads.html:37
#: templates/hyperkitty/fragments/thread_left_nav.html:18
#: templates/hyperkitty/overview.html:75
msgid "All Threads"
msgstr "כל השרשורים"

#: templates/hyperkitty/fragments/overview_top_posters.html:18
msgid "See the profile"
msgstr "הצגת הפרופיל"

#: templates/hyperkitty/fragments/overview_top_posters.html:24
msgid "posts"
msgstr "רשומות"

#: templates/hyperkitty/fragments/overview_top_posters.html:29
msgid "No posters this month (yet)."
msgstr "אין מפרסמים החודש (עדיין)."

#: templates/hyperkitty/fragments/send_as.html:5
msgid "This message will be sent as:"
msgstr "הודעה זו תישלח בתור:"

#: templates/hyperkitty/fragments/send_as.html:6
msgid "Change sender"
msgstr "החלפת מוען"

#: templates/hyperkitty/fragments/send_as.html:16
msgid "Link another address"
msgstr "קישור כתובת נוספת"

#: templates/hyperkitty/fragments/send_as.html:20
msgid ""
"If you aren't a current list member, sending this message will subscribe you."
msgstr "אם לא ערכת מינוי לרשימה הזאת, שליחת הודעה תרשום אותך."

#: templates/hyperkitty/fragments/thread_left_nav.html:11
msgid "List overview"
msgstr "סקירת רשימות"

#: templates/hyperkitty/fragments/thread_left_nav.html:27 views/message.py:75
#: views/mlist.py:102 views/thread.py:167
msgid "Download"
msgstr "הורדה"

#: templates/hyperkitty/fragments/thread_left_nav.html:30
msgid "Past 30 days"
msgstr "30 הימים האחרונים"

#: templates/hyperkitty/fragments/thread_left_nav.html:31
msgid "This month"
msgstr "החודש הזה"

#: templates/hyperkitty/fragments/thread_left_nav.html:34
msgid "Entire archive"
msgstr "כל הארכיון"

#: templates/hyperkitty/index.html:9 templates/hyperkitty/index.html:63
msgid "Available lists"
msgstr "רשימות זמינות"

#: templates/hyperkitty/index.html:22 templates/hyperkitty/index.html:27
#: templates/hyperkitty/index.html:72
msgid "Most popular"
msgstr "הנפוצים ביותר"

#: templates/hyperkitty/index.html:26
msgid "Sort by number of recent participants"
msgstr "סידור לפי מספר המשתתפים לאחרונה"

#: templates/hyperkitty/index.html:32 templates/hyperkitty/index.html:37
#: templates/hyperkitty/index.html:75
msgid "Most active"
msgstr "הפעיל ביותר"

#: templates/hyperkitty/index.html:36
msgid "Sort by number of recent discussions"
msgstr "סידור לפי הדיונים האחרונים"

#: templates/hyperkitty/index.html:42 templates/hyperkitty/index.html:47
#: templates/hyperkitty/index.html:78
msgid "By name"
msgstr "לפי שם"

#: templates/hyperkitty/index.html:46
msgid "Sort alphabetically"
msgstr "סידור לפי אלפבית"

#: templates/hyperkitty/index.html:52 templates/hyperkitty/index.html:57
#: templates/hyperkitty/index.html:81
msgid "Newest"
msgstr "החדש ביותר"

#: templates/hyperkitty/index.html:56
msgid "Sort by list creation date"
msgstr "סידור לפי מועד יצירת הרשימה"

#: templates/hyperkitty/index.html:68
msgid "Sort by"
msgstr "סידור לפי"

#: templates/hyperkitty/index.html:91
msgid "Hide inactive"
msgstr "הסתרת בלתי פעילות"

#: templates/hyperkitty/index.html:92
msgid "Hide private"
msgstr "הסתרת פרטיות"

#: templates/hyperkitty/index.html:99
msgid "Find list"
msgstr "איתור רשימה"

#: templates/hyperkitty/index.html:123 templates/hyperkitty/index.html:193
#: templates/hyperkitty/user_profile/last_views.html:34
#: templates/hyperkitty/user_profile/last_views.html:73
msgid "new"
msgstr "חדשה"

#: templates/hyperkitty/index.html:134 templates/hyperkitty/index.html:204
msgid "private"
msgstr "פרטית"

#: templates/hyperkitty/index.html:136 templates/hyperkitty/index.html:206
msgid "inactive"
msgstr "בלתי פעילה"

#: templates/hyperkitty/index.html:142 templates/hyperkitty/index.html:232
#: templates/hyperkitty/overview.html:91 templates/hyperkitty/overview.html:108
#: templates/hyperkitty/overview.html:178
#: templates/hyperkitty/overview.html:185
#: templates/hyperkitty/overview.html:192
#: templates/hyperkitty/overview.html:201
#: templates/hyperkitty/overview.html:209 templates/hyperkitty/reattach.html:39
#: templates/hyperkitty/thread.html:111
msgid "Loading..."
msgstr "בטעינה…"

#: templates/hyperkitty/index.html:148 templates/hyperkitty/index.html:221
#: templates/hyperkitty/overview.html:100
#: templates/hyperkitty/thread_list.html:36
#: templates/hyperkitty/threads/right_col.html:44
#: templates/hyperkitty/threads/right_col.html:97
#: templates/hyperkitty/threads/summary_thread_large.html:47
msgid "participants"
msgstr "משתתפים"

#: templates/hyperkitty/index.html:153 templates/hyperkitty/index.html:226
#: templates/hyperkitty/overview.html:101
#: templates/hyperkitty/thread_list.html:41
msgid "discussions"
msgstr "דיונים"

#: templates/hyperkitty/index.html:162 templates/hyperkitty/index.html:240
msgid "No archived list yet."
msgstr "אין עדיין רשימות בארכיון."

#: templates/hyperkitty/index.html:174
#: templates/hyperkitty/user_profile/favorites.html:40
#: templates/hyperkitty/user_profile/last_views.html:45
#: templates/hyperkitty/user_profile/profile.html:15
#: templates/hyperkitty/user_profile/subscriptions.html:41
#: templates/hyperkitty/user_profile/votes.html:46
msgid "List"
msgstr "רשימה"

#: templates/hyperkitty/index.html:175
msgid "Description"
msgstr "תיאור"

#: templates/hyperkitty/index.html:176
msgid "Activity in the past 30 days"
msgstr "פעילות ב־30 הימים האחרונים"

#: templates/hyperkitty/list_delete.html:7
msgid "Delete MailingList"
msgstr "מחיקת רשימת דיוור"

#: templates/hyperkitty/list_delete.html:20
msgid "Delete Mailing List"
msgstr "מחיקת רשימת דיוור"

#: templates/hyperkitty/list_delete.html:26
msgid ""
"will be deleted along with all the threads and messages. Do you want to "
"continue?"
msgstr "תימחק על כל השרשורים וההודעות שלה. להמשיך?"

#: templates/hyperkitty/list_delete.html:33
#: templates/hyperkitty/message_delete.html:44
#: templates/hyperkitty/overview.html:78
msgid "Delete"
msgstr "מחיקה"

#: templates/hyperkitty/list_delete.html:34
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
msgid "or"
msgstr "או"

#: templates/hyperkitty/list_delete.html:36
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
#: templates/hyperkitty/user_profile/votes.html:36
#: templates/hyperkitty/user_profile/votes.html:74
msgid "cancel"
msgstr "ביטול"

#: templates/hyperkitty/message.html:22
msgid "thread"
msgstr "שרשור"

#: templates/hyperkitty/message_delete.html:7
#: templates/hyperkitty/message_delete.html:20
msgid "Delete message(s)"
msgstr "מחיקת הודעות"

#: templates/hyperkitty/message_delete.html:25
#, python-format
msgid ""
"\n"
"        %(count)s message(s) will be deleted. Do you want to continue?\n"
"        "
msgstr ""
"\n"
"        %(count)s הודעות תימחקנה. להמשיך?\n"
"        "

#: templates/hyperkitty/message_new.html:8
#: templates/hyperkitty/message_new.html:21
msgid "Create a new thread"
msgstr "יצירת שרשור חדש"

#: templates/hyperkitty/message_new.html:22
#: templates/hyperkitty/user_posts.html:22
msgid "in"
msgstr "בתוך"

#: templates/hyperkitty/message_new.html:52
#: templates/hyperkitty/messages/message.html:145
msgid "Send"
msgstr "שליחה"

#: templates/hyperkitty/messages/message.html:17
#, python-format
msgid "See the profile for %(name)s"
msgstr "הצגת הפרופיל עבור %(name)s"

#: templates/hyperkitty/messages/message.html:27
msgid "Unread"
msgstr "לא נקראה"

#: templates/hyperkitty/messages/message.html:44
msgid "Sender's time:"
msgstr "השעה אצל המוען:"

#: templates/hyperkitty/messages/message.html:50
msgid "New subject:"
msgstr "נושא חדש:"

#: templates/hyperkitty/messages/message.html:61
msgid "Attachments:"
msgstr "קבצים מצורפים:"

#: templates/hyperkitty/messages/message.html:76
msgid "Display in fixed font"
msgstr "הצגה בגופן ברוחב אחיד"

#: templates/hyperkitty/messages/message.html:79
msgid "Permalink for this message"
msgstr "קישור קבוע להודעה זו"

#: templates/hyperkitty/messages/message.html:90
#: templates/hyperkitty/messages/message.html:96
msgid "Reply"
msgstr "תגובה"

#: templates/hyperkitty/messages/message.html:93
msgid "Sign in to reply online"
msgstr "יש להיכנס כדי להגיב דרך כאן"

#: templates/hyperkitty/messages/message.html:105
#, python-format
msgid ""
"\n"
"                %(email.attachments.count)s attachment\n"
"                "
msgid_plural ""
"\n"
"                %(email.attachments.count)s attachments\n"
"                "
msgstr[0] ""
"\n"
"                קובץ אחד מצורף\n"
"                "
msgstr[1] ""
"\n"
"                %(email.attachments.count)s קבצים מצורפים\n"
"                "

#: templates/hyperkitty/messages/message.html:131
msgid "Quote"
msgstr "ציטוט"

#: templates/hyperkitty/messages/message.html:132
msgid "Create new thread"
msgstr "יצירת שרשור חדש"

#: templates/hyperkitty/messages/message.html:135
msgid "Use email software"
msgstr "להשתמש בתכנית דוא״ל"

#: templates/hyperkitty/messages/right_col.html:11
msgid "Back to the thread"
msgstr "חזרה לשרשור"

#: templates/hyperkitty/messages/right_col.html:18
msgid "Back to the list"
msgstr "חזרה לרשימה"

#: templates/hyperkitty/messages/right_col.html:27
msgid "Delete this message"
msgstr "מחיקת ההודעה הזו"

#: templates/hyperkitty/messages/summary_message.html:23
#, python-format
msgid ""
"\n"
"                                by %(name)s\n"
"                            "
msgstr ""
"\n"
"                                מאת %(name)s\n"
"                            "

#: templates/hyperkitty/overview.html:35
msgid "Home"
msgstr "דף הבית"

#: templates/hyperkitty/overview.html:38 templates/hyperkitty/thread.html:78
msgid "Stats"
msgstr "סטטיסטיקה"

#: templates/hyperkitty/overview.html:41
msgid "Threads"
msgstr "שרשורים"

#: templates/hyperkitty/overview.html:47 templates/hyperkitty/overview.html:58
#: templates/hyperkitty/thread_list.html:44
msgid "You must be logged-in to create a thread."
msgstr "עליך להיכנס כדי ליצור שרשור."

#: templates/hyperkitty/overview.html:60
#: templates/hyperkitty/thread_list.html:48
msgid ""
"<span class=\"d-none d-md-inline\">Start a n</span><span class=\"d-md-none"
"\">N</span>ew thread"
msgstr ""
"<span class=\"d-none d-md-inline\">התחלת שרשור </span><span class=\"d-md-none"
"\">ח</span>דש"

#: templates/hyperkitty/overview.html:72
msgid ""
"<span class=\"d-none d-md-inline\">Manage s</span><span class=\"d-md-none"
"\">S</span>ubscription"
msgstr ""
"<span class=\"d-none d-md-inline\">ניהול </span><span class=\"d-md-none\">מ</"
"span>ינוי"

#: templates/hyperkitty/overview.html:88
msgid "Activity Summary"
msgstr "סיכום פעילות"

#: templates/hyperkitty/overview.html:90
msgid "Post volume over the past <strong>30</strong> days."
msgstr "נפח פרסומים ב־<strong>30</strong> הימים האחרונים."

#: templates/hyperkitty/overview.html:95
msgid "The following statistics are from"
msgstr "הסטטיסטיקות הבאות הן מתחילת"

#: templates/hyperkitty/overview.html:96
msgid "In"
msgstr "בתוך"

#: templates/hyperkitty/overview.html:97
msgid "the past <strong>30</strong> days:"
msgstr "<strong>30</strong> הימים האחרונים:"

#: templates/hyperkitty/overview.html:106
msgid "Most active posters"
msgstr "המפרסמים הפעילים ביותר"

#: templates/hyperkitty/overview.html:115
msgid "Prominent posters"
msgstr "מפרסמים עקביים"

#: templates/hyperkitty/overview.html:130
msgid "kudos"
msgstr "תודות"

#: templates/hyperkitty/overview.html:176
msgid "Recently active discussions"
msgstr "דיונים שהיו פעילים לאחרונה"

#: templates/hyperkitty/overview.html:183
msgid "Most popular discussions"
msgstr "הדיונים הנפוצים ביותר"

#: templates/hyperkitty/overview.html:190
msgid "Most active discussions"
msgstr "הדיונים הפעילים ביותר"

#: templates/hyperkitty/overview.html:197
msgid "Discussions You've Flagged"
msgstr "דיונים שסימנת בדגל"

#: templates/hyperkitty/overview.html:205
msgid "Discussions You've Posted to"
msgstr "דיונים שפרסמת אליהם"

#: templates/hyperkitty/reattach.html:9
msgid "Reattach a thread"
msgstr "חיבור שרשור מחדש"

#: templates/hyperkitty/reattach.html:20
msgid "Re-attach a thread to another"
msgstr "חיבור שרשור לשרשור אחר מחדש"

#: templates/hyperkitty/reattach.html:22
msgid "Thread to re-attach:"
msgstr "שרשור לחיבור מחדש:"

#: templates/hyperkitty/reattach.html:29
msgid "Re-attach it to:"
msgstr "חיבור מחדש אל:"

#: templates/hyperkitty/reattach.html:31
msgid "Search for the parent thread"
msgstr "חיפוש שרשור ההורה"

#: templates/hyperkitty/reattach.html:32
msgid "Search"
msgstr "חיפוש"

#: templates/hyperkitty/reattach.html:44
msgid "this thread ID:"
msgstr "מזהה שרשור זה:"

#: templates/hyperkitty/reattach.html:50
msgid "Do it"
msgstr "לבצע"

#: templates/hyperkitty/reattach.html:50
msgid "(there's no undoing!), or"
msgstr "(לא ניתן להתחרט!), או"

#: templates/hyperkitty/reattach.html:52
msgid "go back to the thread"
msgstr "חזרה לשרשור"

#: templates/hyperkitty/search_results.html:8
msgid "Search results for"
msgstr "תוצאות לחיפוש אחר"

#: templates/hyperkitty/search_results.html:30
msgid "search results"
msgstr "תוצאות חיפוש"

#: templates/hyperkitty/search_results.html:32
msgid "Search results"
msgstr "תוצאות חיפוש"

#: templates/hyperkitty/search_results.html:34
msgid "for query"
msgstr "לשאילתה"

#: templates/hyperkitty/search_results.html:44
#: templates/hyperkitty/threads/right_col.html:49
#: templates/hyperkitty/user_posts.html:34
msgid "messages"
msgstr "הודעות"

#: templates/hyperkitty/search_results.html:57
msgid "sort by score"
msgstr "סידור לפי דירוג"

#: templates/hyperkitty/search_results.html:60
msgid "sort by latest first"
msgstr "סידור כך שהאחרונים בהתחלה"

#: templates/hyperkitty/search_results.html:63
msgid "sort by earliest first"
msgstr "סידור כך שהישנים ביותר בהתחלה"

#: templates/hyperkitty/search_results.html:84
msgid "Sorry no email could be found for this query."
msgstr "לא נמצאה אף הודעה בשאילתה הזו, סליחה."

#: templates/hyperkitty/search_results.html:87
msgid "Sorry but your query looks empty."
msgstr "נראה שהשאילתה שלך ריקה, עמך הסליחה."

#: templates/hyperkitty/search_results.html:88
msgid "these are not the messages you are looking for"
msgstr "אלו לא ההודעות שחיפשת"

#: templates/hyperkitty/thread.html:30
msgid "newer"
msgstr "חדשות יותר"

#: templates/hyperkitty/thread.html:44
msgid "older"
msgstr "ישנות יותר"

#: templates/hyperkitty/thread.html:72
msgid "First Post"
msgstr "פרסום ראשון"

#: templates/hyperkitty/thread.html:75
#: templates/hyperkitty/user_profile/favorites.html:45
#: templates/hyperkitty/user_profile/last_views.html:50
msgid "Replies"
msgstr "תגובות"

#: templates/hyperkitty/thread.html:97
msgid "Show replies by thread"
msgstr "הצגת תגובות לפי שרשור"

#: templates/hyperkitty/thread.html:100
msgid "Show replies by date"
msgstr "הצגת תגובות לפי תאריך"

#: templates/hyperkitty/thread_list.html:56
msgid "Sorry no email threads could be found"
msgstr "לא ניתן למצוא שרשורי הודעות, עמך הסליחה"

#: templates/hyperkitty/threads/category.html:7
msgid "Click to edit"
msgstr "ללחוץ לעריכה"

#: templates/hyperkitty/threads/category.html:9
msgid "You must be logged-in to edit."
msgstr "עליך להיכנס כדי לערוך."

#: templates/hyperkitty/threads/category.html:15
msgid "no category"
msgstr "אין קטגוריה"

#: templates/hyperkitty/threads/right_col.html:12
msgid "days inactive"
msgstr "ימים של חוסר פעילות"

#: templates/hyperkitty/threads/right_col.html:18
msgid "days old"
msgstr "ימי ותק"

#: templates/hyperkitty/threads/right_col.html:40
#: templates/hyperkitty/threads/summary_thread_large.html:52
msgid "comments"
msgstr "הערות"

#: templates/hyperkitty/threads/right_col.html:48
msgid "unread"
msgstr "לא נקראה"

#: templates/hyperkitty/threads/right_col.html:59
msgid "You must be logged-in to have favorites."
msgstr "עליך להיכנס כדי שיהיו לך מועדפים."

#: templates/hyperkitty/threads/right_col.html:60
msgid "Add to favorites"
msgstr "הוספה למועדפים"

#: templates/hyperkitty/threads/right_col.html:62
msgid "Remove from favorites"
msgstr "הסרה מהמועדפים"

#: templates/hyperkitty/threads/right_col.html:71
msgid "Reattach this thread"
msgstr "חיבור מחדש לשרשור הזה"

#: templates/hyperkitty/threads/right_col.html:75
msgid "Delete this thread"
msgstr "מחיקת השרשור הזה"

#: templates/hyperkitty/threads/right_col.html:113
msgid "Unreads:"
msgstr "לא נקראו:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "Go to:"
msgstr "מעבר אל:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "next"
msgstr "הבא"

#: templates/hyperkitty/threads/right_col.html:116
msgid "prev"
msgstr "הקודם"

#: templates/hyperkitty/threads/summary_thread_large.html:21
#: templates/hyperkitty/threads/summary_thread_large.html:23
msgid "Favorite"
msgstr "סימון כמועדף"

#: templates/hyperkitty/threads/summary_thread_large.html:29
#, python-format
msgid ""
"\n"
"                    by %(name)s\n"
"                    "
msgstr ""
"\n"
"                    מאת %(name)s\n"
"                    "

#: templates/hyperkitty/threads/summary_thread_large.html:39
msgid "Most recent thread activity"
msgstr "הפעילות האחרונה בשרשור"

#: templates/hyperkitty/threads/tags.html:3
msgid "tags"
msgstr "תגיות"

#: templates/hyperkitty/threads/tags.html:9
msgid "Search for tag"
msgstr "חיפוש לפי תגית"

#: templates/hyperkitty/threads/tags.html:15
msgid "Remove"
msgstr "הסרה"

#: templates/hyperkitty/user_posts.html:8
#: templates/hyperkitty/user_posts.html:21
#: templates/hyperkitty/user_posts.html:25
msgid "Messages by"
msgstr "הודעות מאת"

#: templates/hyperkitty/user_posts.html:38
#, python-format
msgid "Back to %(fullname)s's profile"
msgstr "חזרה לפרופיל של %(fullname)s"

#: templates/hyperkitty/user_posts.html:48
msgid "Sorry no email could be found by this user."
msgstr "לא ניתן למצוא אף הודעות שנשלחו על ידי המשתמש הזה, סליחה."

#: templates/hyperkitty/user_profile/base.html:5
#: templates/hyperkitty/user_profile/base.html:12
msgid "User posting activity"
msgstr "פעילות הפרסום של המשתמש"

#: templates/hyperkitty/user_profile/base.html:12
#: templates/hyperkitty/user_public_profile.html:7
#: templates/hyperkitty/user_public_profile.html:14
msgid "for"
msgstr "למשך"

#: templates/hyperkitty/user_profile/base.html:22
msgid "Favorites"
msgstr "מועדפים"

#: templates/hyperkitty/user_profile/base.html:26
msgid "Threads you have read"
msgstr "שרשורים שקראת"

#: templates/hyperkitty/user_profile/base.html:30
#: templates/hyperkitty/user_profile/profile.html:18
#: templates/hyperkitty/user_profile/subscriptions.html:45
msgid "Votes"
msgstr "הצבעות"

#: templates/hyperkitty/user_profile/base.html:34
msgid "Subscriptions"
msgstr "מינויים"

#: templates/hyperkitty/user_profile/favorites.html:24
#: templates/hyperkitty/user_profile/last_views.html:27
#: templates/hyperkitty/user_profile/votes.html:23
msgid "Original author:"
msgstr "מוען מקורי:"

#: templates/hyperkitty/user_profile/favorites.html:26
#: templates/hyperkitty/user_profile/last_views.html:29
#: templates/hyperkitty/user_profile/votes.html:25
msgid "Started on:"
msgstr "מועד ההתחלה:"

#: templates/hyperkitty/user_profile/favorites.html:28
#: templates/hyperkitty/user_profile/last_views.html:31
msgid "Last activity:"
msgstr "פעילות אחרונה:"

#: templates/hyperkitty/user_profile/favorites.html:30
#: templates/hyperkitty/user_profile/last_views.html:33
msgid "Replies:"
msgstr "תגובות:"

#: templates/hyperkitty/user_profile/favorites.html:41
#: templates/hyperkitty/user_profile/last_views.html:46
#: templates/hyperkitty/user_profile/profile.html:16
#: templates/hyperkitty/user_profile/votes.html:47
msgid "Subject"
msgstr "נושא"

#: templates/hyperkitty/user_profile/favorites.html:42
#: templates/hyperkitty/user_profile/last_views.html:47
#: templates/hyperkitty/user_profile/votes.html:48
msgid "Original author"
msgstr "מוען מקורי"

#: templates/hyperkitty/user_profile/favorites.html:43
#: templates/hyperkitty/user_profile/last_views.html:48
#: templates/hyperkitty/user_profile/votes.html:49
msgid "Start date"
msgstr "מועד התחלה"

#: templates/hyperkitty/user_profile/favorites.html:44
#: templates/hyperkitty/user_profile/last_views.html:49
msgid "Last activity"
msgstr "פעילות אחרונה"

#: templates/hyperkitty/user_profile/favorites.html:71
msgid "No favorites yet."
msgstr "אין מועדפים עדיין."

#: templates/hyperkitty/user_profile/last_views.html:22
#: templates/hyperkitty/user_profile/last_views.html:59
msgid "New comments"
msgstr "הערות חדשות"

#: templates/hyperkitty/user_profile/last_views.html:82
msgid "Nothing read yet."
msgstr "עדיין לא נקרא דבר."

#: templates/hyperkitty/user_profile/profile.html:9
msgid "Last posts"
msgstr "רשומות אחרונות"

#: templates/hyperkitty/user_profile/profile.html:17
msgid "Date"
msgstr "תאריך"

#: templates/hyperkitty/user_profile/profile.html:19
msgid "Thread"
msgstr "שרשור"

#: templates/hyperkitty/user_profile/profile.html:20
msgid "Last thread activity"
msgstr "פעילות אחרונה בשרשור"

#: templates/hyperkitty/user_profile/profile.html:49
msgid "No posts yet."
msgstr "אין רשומות עדיין."

#: templates/hyperkitty/user_profile/subscriptions.html:24
msgid "since first post"
msgstr "מאז הרשומה הראשונה"

#: templates/hyperkitty/user_profile/subscriptions.html:26
#: templates/hyperkitty/user_profile/subscriptions.html:63
msgid "post"
msgstr "רשומה"

#: templates/hyperkitty/user_profile/subscriptions.html:31
#: templates/hyperkitty/user_profile/subscriptions.html:69
msgid "no post yet"
msgstr "אין רשומות עדיין"

#: templates/hyperkitty/user_profile/subscriptions.html:42
msgid "Time since the first activity"
msgstr "הזמן מאז הפעילות הראשונה"

#: templates/hyperkitty/user_profile/subscriptions.html:43
msgid "First post"
msgstr "רשומה ראשונה"

#: templates/hyperkitty/user_profile/subscriptions.html:44
msgid "Posts to this list"
msgstr "פרסומים לרשימה הזו"

#: templates/hyperkitty/user_profile/subscriptions.html:76
msgid "no subscriptions"
msgstr "אין מינויים"

#: templates/hyperkitty/user_profile/votes.html:32
#: templates/hyperkitty/user_profile/votes.html:70
msgid "You like it"
msgstr "אהבת את זה"

#: templates/hyperkitty/user_profile/votes.html:34
#: templates/hyperkitty/user_profile/votes.html:72
msgid "You dislike it"
msgstr "לא אהבת את זה"

#: templates/hyperkitty/user_profile/votes.html:50
msgid "Vote"
msgstr "הצבעה"

#: templates/hyperkitty/user_profile/votes.html:83
msgid "No vote yet."
msgstr "אין הצבעה עדיין."

#: templates/hyperkitty/user_public_profile.html:7
msgid "User Profile"
msgstr "פרופיל משתמש"

#: templates/hyperkitty/user_public_profile.html:14
msgid "User profile"
msgstr "פרופיל משתמש"

#: templates/hyperkitty/user_public_profile.html:23
msgid "Name:"
msgstr "שם:"

#: templates/hyperkitty/user_public_profile.html:28
msgid "Creation:"
msgstr "יצירה:"

#: templates/hyperkitty/user_public_profile.html:33
msgid "Votes for this user:"
msgstr "הצבעות למשתמש זה:"

#: templates/hyperkitty/user_public_profile.html:41
msgid "Email addresses:"
msgstr "כתובות דוא״ל:"

#: views/message.py:76
msgid "This message in gzipped mbox format"
msgstr "הודעה זו ארוזה ב־gzip בתצורת mbox"

#: views/message.py:200
msgid "Your reply has been sent and is being processed."
msgstr "התגובה שלך נשלחה והיא עוברת עיבוד."

#: views/message.py:204
msgid ""
"\n"
"  You have been subscribed to {} list."
msgstr ""
"\n"
"  נרשמת לרשימה {}."

#: views/message.py:287
#, python-format
msgid "Could not delete message %(msg_id_hash)s: %(error)s"
msgstr "לא ניתן למחוק את ההודעה %(msg_id_hash)s:‏ %(error)s"

#: views/message.py:296
#, python-format
msgid "Successfully deleted %(count)s messages."
msgstr "%(count)s הודעות נמחקו בהצלחה."

#: views/mlist.py:88
msgid "for this month"
msgstr "לחודש הזה"

#: views/mlist.py:91
msgid "for this day"
msgstr "ליום הזה"

#: views/mlist.py:103
msgid "This month in gzipped mbox format"
msgstr "החודש הזה ב־gzip בתצורת mbox"

#: views/mlist.py:200 views/mlist.py:224
msgid "No discussions this month (yet)."
msgstr "אין דיונים החודש (בינתיים)."

#: views/mlist.py:212
msgid "No vote has been cast this month (yet)."
msgstr "לא התקבלו הצבעות החודש (בינתיים)."

#: views/mlist.py:241
msgid "You have not flagged any discussions (yet)."
msgstr "לא סימנת בדגל אף דיון (בינתיים)."

#: views/mlist.py:264
msgid "You have not posted to this list (yet)."
msgstr "לא פרסמת לרשימה זה (בינתיים)."

#: views/mlist.py:353
msgid "You must be a staff member to delete a MailingList"
msgstr "עליך להיות חלק מהסגל כדי למחוק רשימת דיוור"

#: views/mlist.py:367
msgid "Successfully deleted {}"
msgstr "%(count)s נמחקה בהצלחה"

#: views/search.py:115
#, python-format
msgid "Parsing error: %(error)s"
msgstr "שגיאת פענוח: %(error)s"

#: views/thread.py:168
msgid "This thread in gzipped mbox format"
msgstr "שרשור זה ב־gzip בתצורת mbox"

#~ msgid "Go to"
#~ msgstr "מעבר אל"

#~ msgid "More..."
#~ msgstr "עוד…"

#~ msgid "Discussions"
#~ msgstr "דיונים"

#~ msgid "most recent"
#~ msgstr "העדכניים ביותר"

#~ msgid "most popular"
#~ msgstr "הנפוצים ביותר"

#~ msgid "most active"
#~ msgstr "הפעילים ביותר"

#~ msgid "Update"
#~ msgstr "עדכון"

#, python-format
#~ msgid ""
#~ "\n"
#~ "                                        by %(name)s\n"
#~ "                                    "
#~ msgstr ""
#~ "\n"
#~ "                                        מאת %(name)s\n"
#~ "                                    "
